/*
 * grunt-copy-public
 *
 *
 * Copyright (c) 2014 Allert Schallenberg
 * Licensed under the MIT license.
 */

'use strict';

module.exports = function(grunt) {

    // Please see the Grunt documentation for more information regarding task
    // creation: http://gruntjs.com/creating-tasks

    grunt.registerMultiTask('copy_public', 'Copy assets from Bower components to a flattened structure in a public dir.', function() {
        var options = this.options({
            useComponentName: true
        });

        // Check for 'directory' element in .bowerrc.
        // If it's not there, assume './bower_components/'
        if(grunt.file.isFile('.bowerrc')) {
            var directory = grunt.file.readJSON('.bowerrc').directory || 'bower_components';
        } else {
            var directory = 'bower_components';
        }

        var src = String(this.data.src).replace('_BOWERDIR_', directory);
        var dest_dir = this.data.dest || 'public';

        // Find files in public folder of each Bower component
        var files = grunt.file.expand({filter: 'isFile'}, src);

        var current_component = false;
        var copied = 0;

        // Loop over files found in public dirs
        for(var i in files) {

            if (dest_dir.indexOf('$') > -1) {
                var subject = files[i];

                var lastSlash = subject.lastIndexOf('/') + 1;

                var path = subject.substring(0, lastSlash);
                var filename = subject.substring(lastSlash);

                var regex = '^' + src.replace(/\*\*/g, '(.*?)') + '/$';

                var dest = path.replace(new RegExp(regex), dest_dir) + '/' + filename;

                try {
                    grunt.file.copy(files[i], dest);
                    copied++;
                }
                catch(e) {
                    grunt.log.error('Error copying file ' + dest);
                }
                continue;

            } else {

                var start = files[i].split('/');
                var end = start.splice(start.indexOf('public')).slice(1);
                var component = start.slice(-1)[0];


                // Use component name as subfolder of asset type folder (eg. ./fonts/component-name/filename.ext)
                if(options.useComponentName) {
                    end.splice(1, 0, component);
                }

                end.unshift(dest_dir);

                if(component !== current_component) {
                    if(current_component) {
                        grunt.log.ok('Copied ' + copied + ' files(s)');
                    }

                    current_component = component;
                    grunt.log.subhead('Found component ' + component);
                    copied = 0;
                }

                // Join parts to assemble destination path + filename
                var dest = end.join('/');
            }

            try {
                grunt.file.copy(files[i], dest);
                copied++;
            } catch(e) {
                grunt.log.error('Error copying file ' + dest);
            }
        }

        grunt.log.ok('Copied ' + copied + ' files(s)');

    });

};
